##' @title Decompose the coefficient of determination (R^2)
##'
##' @description Decompose the coefficient of determination (R^2) of the model with `response` as response and `predictors` as predictors, such that each predictor gets a share of the R^2 of the model.
##' This creates a measure of relative variable importance which can be used, in addition to P-values, to interpret the model.
##' The relative importances have the properties that they will always sum to the model R^2 (proper decomposition), are always non-negative (non-negativity) and if a predictor has a non-zero coefficient in the model it will not get a zero share (inclusion).
##'
##' @param formula is a formula specifying the model
##' @param data data-frame, contains data used to fit the model. All data referred to in 'response' and 'predictors' need to be present in 'data'.
##' The random intercepts should have it's column name on the form "*name*", not "(1 | *name*)".
##' @param weights weights vector passed to `stats::lm` and `lme4::lmer` when fitting models.
##' @param method Character string saying which method to use to decompose R^2.
##' Available methods are the LMG method ("lmg") and the relative weights ("rw") method.
##' @return Named vector containing the relative variable importances of the predictors of the specified model.
##' @author Andreas Matre
decompR2_internal <- function(formula,
                     data,
                     weights = NULL,
                     method = NULL) {
  RW_limit <- 15 # Number of fixed effects to accept before using the relative weights method
  methods_lmg <- c("lmg")
  methods_rw <- c("rw", "relative weights")

  t <- stats::terms(formula, data = data)
  if(any(attr(t, "order") > 1)) {
    stop(paste0("'decompR2' does not support interactions"))
  }
  if(attr(t, "response") == 0) {
    stop("There needs to be a response (left hand term) in the formula")
  }

  response <- as.character(attr(t, "variables")[attr(t, "response") + 1])
  predictors <- attr(t, "term.labels")

  random_intercepts <- grepl(pattern = '|', x = predictors, fixed = TRUE)
  fixed_effects <- !random_intercepts

  predictors[random_intercepts] <- paste0("(", predictors[random_intercepts], ")")

  # If there is only one predictor in the model, just return the R^2 of the model.
  if(ncol(data) == 2 & !any(random_intercepts)) {
    model <- fit_model(formula = as.formula(paste0(response, " ~ .")), data = data, weights = weights)
    R2 <- calc_R2(model)
    names(R2) <- predictors
    return(R2)
  }
  if(ncol(data) == 2 & any(random_intercepts)) {
    model <- fit_model(formula = as.formula(paste0(response, " ~ ", predictors)), data = data, weights = weights)
    R2 <- calc_R2(model)
    names(R2) <- predictors
    return(R2)
  }

  if(is.null(method)) {
    if(sum(fixed_effects) >= RW_limit) {
      method <- "rw"
    } else {
      method <- "lmg"
    }
  }

  if(tolower(method) %in% methods_lmg) {
    return(decompR2_lmg(response = response, predictors = predictors, data = data, weights = weights))
  } else if(tolower(method) %in% methods_rw) {
    return(decompR2_rw(response = response, predictors = predictors, data = data, weights = weights))
  } else {
    stop(paste0("Invalid `methods` value. Valid values are: ", methods_lmg, " for LMG and: ", methods_rw, " for relative weights."))
  }
}

##' @title Decompose the coefficient of determination (R^2)
##'
##' @description Decompose the coefficient of determination (R^2) of a linear regression or linear random intercept model such that each predictor gets a share of the R^2 of the model.
##'              This creates a measure of relative variable importance which can be used, as a supplement to p-values, to interpret the model.
##'
##' @details Creates a decomposition of the R^2 of a model, i.e., distribute the R^2 to the predictors of the model such that each predictor gets a share corresponding to it's contribution to the model R^2. These shares are called the relative variable importances of the predictors.
##'          The relative importances have the properties that they will always sum to the model R^2 (proper decomposition), are always non-negative (non-negativity) and if a predictor has a non-zero coefficient in the model it will not get a zero share (inclusion).
##'          There are two methods currently implemented to perform the decomposition, the LMG method and the relative weights method.
##'
##'          The LMG method is the most accurate one, but also the most computationally expensive. It is therefore the default method if there are less than 15 fixed effects in the model.
##'          The LMG method works by looking at all permutations of the predictors, i.e., all the orderings of the predictors. Then, fit models with just the first predictor in the permutation, the first two predictors in the permutation, the first three predictors in the permutation, and so on.
##'          For a specific predictor, it's share is calculated by looking at the increase in R^2 of the model when that predictor is added to the model. Then take the mean of this increase when looking at all permutations of predictors.
##'
##'          The relative weights method is not as accurate, but much more computationally efficient. It is therefore the default method when there are more than 15 fixed effects in the model.
##'          The relative weights method works by first performing a linear transforming on the numeric fixed effects such that new numeric fixed effects are calculated which are uncorrelated.
##'          In a model with only uncorrelated standardized numeric fixed effects the relative importance of each predictor is simply it's squared coefficient.
##'          If there are categorical predictors or random intercepts in the model it is more complicated. Then a similar approach is used as in the LMG method, where the mean increase in R^2 is considered for each predictor, but now the transformed numerical fixed effects are considered as one "block" which is either all in the model or none are in the model.
##'          The categorical predictors and random intercepts get shares the same way is in the LMG method, the mean increase in model R^2 when they are added to the model.
##'          For the numerical fixed effect, look at the mean increase in R^2 when all the numerical fixed effects are added to the model. Then use the squared coefficients to distribute this increase to each numerical fixed effect.
##'          Having the numerical fixed effects as a "block" reduces the number of possible permutations dramatically, thus reducing the computational complexity.
##'
##'          There might be some warnings about models failing to converge when there are random intercepts in the model. This likely comes from the fact that for some combinations of predictors there is not enough difference between the different clusters, which gives `lme4::lmer()` problems when fitting. This failed convergence, luckily, only seems to happen for few of the fitted models, which means that it should not affect the results too much, since the result is an average of many values.
##'
##' @param obj is a model object from either `stats::lm` or `lme4::lmer`
##'
##' OR
##'
##' a formula, e.g., y ~ x1 + x2 + (1 | group)
##' @param data only if `obj` is a formula. data-frame, contains data used to fit the model. All variables referred to in the formula in `obj` need to be present as columns in 'data'.
##'             The random intercepts should have it's column name on the form "*name*", not "(1 | *name*)".
##' @param subset only if `obj` is a formula. Is passed to `stats::model.frame` to subset data. A specification of the rows to be used: defaults to all rows.
##'               This can be any valid indexing vector (see [.data.frame)
##'               for the rows of `data` or if that is not supplied, a data
##'               frame made up of the variables used in formula. For more details see `?stats::model.frame`
##' @param na.action only if `obj` is a formula. Is passed to `stats::model.frame`. Is how `NA`s are treated.  The default is first, any
##'          `na.action` attribute of `data`, second a `na.action` setting
##'          of options, and third na.fail if that is unset.  The
##'          `factory-fresh` default is na.omit.  Another possible value
##'          is `NULL`. For mode details see `?stats::model.frame`
##' @param weights only if `obj` is a formula. Is passed to `stats::lm` or `lme4::lmer`, depending on if there are random intercepts in the formula. See `?stats::lm` and `?lme4::lmer` for more details.
##' @param method is a character string saying which method to use to decompose R^2.
##' Available methods are the LMG method ("lmg") and the relative weights ("rw") method. The default is to use the LMG method if there are less than 15 fixed effects in the model and otherwise use the relative weights method, as it is more computationally efficient.
##' @param ... currently not used, only here to satisfy S3 generic requirements
##' @return Named vector containing the relative variable importances of the predictors of the specified model.
##' @author Andreas Matre
##' @aliases decompR2.formula decompR2.lm decompR2.lmerMod
##'
##' @examples
##' data(cake, package = "lme4")
##'
##' decompR2(angle ~ temp + (1 | recipe) + (1 | replicate), data = cake)
##' decompR2(angle ~ temp + (1 | recipe) + (1 | replicate), data = cake, method = "rw")
##'
##' m <- lme4::lmer(angle ~ temp + (1 | recipe) + (1 | replicate), data = cake)
##' decompR2(m)
##'
##' @export
decompR2 <- function(obj, ...) {
  UseMethod("decompR2", obj)
}


##' @rdname decompR2
##' @export
decompR2.formula <- function(obj,
                 data,
                 subset,
                 weights,
                 na.action,
                 method = NULL,
                 ...) {
  t <- stats::terms(obj, data = data)

  response <- as.character(attr(t, "variables")[attr(t, "response") + 1])
  predictors <- attr(t, "term.labels")
  fixed_effects <- predictors[!grepl(pattern = '|', x = predictors, fixed = TRUE)]
  if(any(grepl(pattern = '|', x = predictors, fixed = TRUE))) { # Check for random effects
    lf <- lme4::lFormula(obj, data = data)

    # Check for any random slopes
    if(any(!sapply(lf$reTrms$cnms, function(x) identical(x, "(Intercept)")))) {
      stop("'decompR2' only supports random intercepts as random effects")
    }

    random_intercept_cols <- names(lf$reTrms$cnms)
  } else {
    random_intercept_cols <- c()
  }


  # Use model.frame to do subsets and remove NA's according to na.action and create columns for the transformed variables in the formula
  model.frame_args <- list()
  model.frame_args$formula <- stats::as.formula(paste0(response, " ~ ", paste0(c(fixed_effects, random_intercept_cols), collapse = " + ")))
  model.frame_args$data <- data
  if(!missing(na.action)) {
    model.frame_args$na.action <- na.action
  }
  if(!missing(subset)) {
    model.frame_args$subset <- subset
  }

  data <- do.call(stats::model.frame, model.frame_args)

  #browser()
  if(missing(weights)) {
    weights <- NULL
  }

  return(decompR2_internal(formula = obj, data = data,
                           method = method, weights = weights))
}

##' @rdname decompR2
##' @export
decompR2.lm <- function(obj,
            method = NULL,
            ...) {
  f <- stats::formula(obj)
  data <- stats::model.frame(obj)
  args <- list(formula = f, data = data, method = method)
  if(!is.null(stats::weights(obj))) {
    args[["weights"]] <- stats::weights(obj)
  }
  return(do.call(decompR2_internal, args))
  #return(decompR2.formula(obj = f, data = data, weights = weights, na.action = na.action, method = method))
}

##' @rdname decompR2
##' @export
decompR2.lmerMod <- function(obj,
            method = NULL,
            ...) {
  if(!identical(stats::family(obj)$family, "gaussian") || !identical(stats::family(obj)$link, "identity")) {
    stop("'decompR2' only supports LMMs, i.e., the family of the model must be gaussian and the link function must be the identity.")
  }
  if(any(!sapply(obj@cnms, function(x) identical(x, "(Intercept)")))) {
    stop("'decompR2' only supports random intercepts as random effects")
  }

  f <- stats::formula(obj)
  data <- stats::model.frame(obj)
  weights <- stats::weights(obj) # If there are no weights the default for `lme4::lmer` is a vector of 1's, so don't need to check if it exists as for `lm` objects.
  return(decompR2_internal(formula = f, data = data, weights = weights, method = method))
}
