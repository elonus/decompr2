% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utility.R
\name{get_fixed_coef}
\alias{get_fixed_coef}
\title{Get the coefficients of the fixed effects}
\usage{
get_fixed_coef(model)
}
\arguments{
\item{model}{is a model from either `stats::lm` or `lme4::lmer`}
}
\value{
named vector containing the coefficients of the fixed effects of `model`.
}
\description{
Gets the coefficients of the fixed effects from either a `stats::lm` or `lme4::lmer` model.
}
\author{
Andreas Matre
}
